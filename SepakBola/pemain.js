// Deklarasi Class
class Pemain {
  // Deklarasi Constructor dan Parameternya
  constructor(nama, noPunggung, posisi, gaji) {
    this.nama = nama;
    this.noPunggung = noPunggung;
    this.posisi = posisi;
    this.gaji = gaji;
  }

  // Method-Method dan Menampilkan data dengan console.log
  passing(rekan) {
    console.log(rekan, 'mengoper bola ke ', this.nama);
  }

  dribbling(lawan) {
    console.log(this.nama, 'melakukan dribble dan berhasil melewati', lawan);
  }

  shooting(keeper) {
    console.log(keeper, 'harus berhati-hati karena', this.nama, 'akan melakukan shooting');
  }

  gol(score) {
    console.log('Dan Gooooo......llll', this.nama, 'dengan nomor punggung', this.noPunggung, 'Berhasil menambah score menjadi', score + '-0');
  }
}

// Module Export
module.exports = Pemain;
